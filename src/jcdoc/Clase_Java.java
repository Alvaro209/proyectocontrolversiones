package jcdoc;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * Esto es para javadoc
 * 
 * @author DAW109
 * 
 * @version 1.0
 * 
 * @see <a href="http://chamilo1617.iesmiguelherrero.com/">http://chamilo1617.iesmiguelherrero.com/</a>
 */
public class Clase_Java {  
    
    /**
     * variable privada 
     */

  
    private final String Autor = "jc Mouse";
   
    public String Frase_del_dia = "Carpe diem";   
    
    
   
    public Clase_Java(){
        System.out.println( this.Autor + " te aconseja '" + this.Frase_del_dia + "'" );
    }
    
    
    /**
     * 
     * @param value es el valor de la fecha
     * @return la fecha
     */

    
    public Date StringToDate( String value )
    {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
         try {
            date = (java.util.Date) formatter.parse( value );
        } catch (ParseException ex) {
            System.err.println( ex.getMessage() );
        }
        return date;
        
    }
    
    /**
     * 
     * @param a es el primer numero 
     * @param b es el segundo numero
     * @return resultado de la suma de a+b
     */
    
    
    public int Suma( int a , int b)
    {
        return a + b;
    }
     
    public int suma_enteros( int a, int b)
    {
        int resultado = 0;
        if( a>0 && b>0)
            resultado = a + b;
        return resultado;
    }
	
	public int Resta( int a , int b)
    {
        return a - b;
    }

}
